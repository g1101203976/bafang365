"ui";

//2022.01.15    增加防止成员数更新导致的报错
//2021.09.15    打开自动刷新时屏蔽手动刷新
//2021.09.13    增加张磊的token
//2021.09.09    增加信息筛选条件
//2021.09.08    优化信息的排列，优化了动画结束
//2021.09.07    增加了文字流动效果（待优化）
//2021.09.06    增加了WEB+APP随机刷新，增加了刷新的随机延迟
/////////////////////////////////////////////////////////////
//导入需要的类
importClass(android.graphics.Color);
importClass(android.graphics.LinearGradient);
importClass(android.graphics.Shader);
importClass(android.graphics.Matrix);
importClass(android.animation.ValueAnimator);
/////////////////////////////////////////////////////////////
//定义变量1
var chengyuan_phone = ["18621629494", "13621589031"];
var chengyuan_token = [
  "1Djbm+kXzPFCgRHkLSXTCDk2YTNhNWYzZDM4ZmNjMjkyYjM0MTQ3ODkxMzdmMmM2NzkxODk2ODhhNzYzYTRmNWM1MGY5NjMwMGMxY2QzNTQbtSB5nMsO9OdD/cCRoSE99uYJuBbRVOvpa6rSkwFQz/ethMfP1atOvmSkKhnVZpM=",
  "eF2Qfw8Ih8s+8AeaRPkpyTMzODI4YmUyZjU3ZmQwOTRhMzgwZjYwMzRmZWRlY2ZkZWRmMjVjMTk0ZTliM2I1MTU3ZGEyYjMwMjUxOGVjOGI92JJs8bUvJodxaINGg9Aw2jLrpiKShWUK2Oge17xzX+Ez/xrvCu7AkRa9WzfE1ns=",
];
var 手机号 = "";
var token = "";
var appUpdataUrl = "//";
/////////////////////////////////////////////////////////////
//定义变量2
var url1 =
  "http://newrent.house365.com/api/tf-app/get-sell-list?city=nj&client=tf&deviceid=E0%3ADC%3AFF%3AFB%3AA4%3AD3&version=8.2.82&v=8.2.82&commitId=38c2795&api_key=android&tt=1629447201803&infotype=1&name=HouseSell&pagesize=20&guid=4126443&infofrom=1&page=1&telno=17512573036&order=creattime&desc=DESC";
var url2 = "http://nj.sell.house365.com/district/f1-n1-x1.html";
var xinxi = "";
var num = "";
var 信息 = "";
var yemian_信息 = "";
var 姓名 = "";
var phone_num = "";
var 运行次数 = 0;
var 序号 = 1;
var id2_on = false;
var apporweb = 0;
var yanmianrizhi;
var num_stroge = [];
var animatorList = [];
var version = files.read("./version.js");
/////////////////////////////////////////////////////////////
//界面生成
ui.statusBarColor("#50000000");
ui.layout(
  <vertical bg="#eeeeec">
    <card
      w="*"
      h="100"
      margin="10 5"
      cardElevation="1dp"
      foreground="?selectableItemBackground"
    >
      <View bg="#4caf50" h="*" w="5" />
      <View bg="#4caf50" layout_gravity="right" h="*" w="1" />
      <horizontal gravity="center">
        <text
          id="xinxi1"
          w="auto"
          h="auto"
          text=""
          gravity="center"
          textColor="#4caf50"
          textSize="20"
          line="3"
        />
      </horizontal>
    </card>
    <card
      w="*"
      h="60"
      margin="10 5"
      cardElevation="1dp"
      foreground="?selectableItemBackground"
    >
      <View bg="#4caf50" h="*" w="5" />
      <View bg="#4caf50" layout_gravity="right" h="*" w="1" />
      <horizontal gravity="center">
        <text
          id="xinxi2"
          w="auto"
          h="auto"
          text="业主姓名"
          textColor="#4caf50"
          textSize="20"
        />
      </horizontal>
    </card>
    <card
      w="*"
      h="200"
      margin="10 5"
      cardElevation="1dp"
      foreground="?selectableItemBackground"
    >
      <View bg="#4caf50" h="*" w="5" />
      <View bg="#4caf50" layout_gravity="right" h="*" w="1" />
      <horizontal gravity="center">
        <frame>
          <text id="占位" padding="10" paddingTop="25" />
          <img
            id="tu"
            src="http://www.house365.com/images/website/logo_new.png"
          />
        </frame>
        <vertical>
          <text
            id="phone_num"
            text="虚拟号码"
            gravity="center|top"
            padding="10"
            line="1"
          />
          <button
            id="call"
            text="拨打电话"
            gravity="center|bottom"
            layout_weight="2"
            style="Widget.AppCompat.Button.Borderless.Colored"
            textColor="#4caf50"
          />
        </vertical>
      </horizontal>
    </card>
    <card
      w="*"
      h="60"
      margin="10 5"
      cardElevation="1dp"
      foreground="?selectableItemBackground"
    >
      <View bg="#6caf50" h="*" w="5" />
      <View bg="#6caf50" layout_gravity="right" h="*" w="1" />
      <horizontal gravity="center">
        <Switch
          id="zidong"
          layout_weight="1"
          layout_gravity="center"
          text="        自动刷新"
          textColor="#4caf50"
          textSize="18"
        />
        <button
          id="shoudong"
          layout_weight="2"
          layout_gravity="center"
          style="Widget.AppCompat.Button.Borderless.Colored"
          text="手动刷新一次"
          textColor="#4caf50"
          textSize="18"
        />
      </horizontal>
    </card>
    <linear h="auto">
      <button
        id="tolog"
        w="60"
        h="50"
        layout_gravity="center"
        text="全部日志"
        textSize="13"
        style="Widget.AppCompat.Button.Borderless.Colored"
      />
      <button
        id="clean"
        w="60"
        h="50"
        layout_gravity="center"
        text="清空日志"
        textSize="13"
        style="Widget.AppCompat.Button.Borderless.Colored"
      />
      <vertical>
        <Switch
          id="boda"
          text="自动拨打"
          layout_gravity="center"
          textColor="#4caf50"
          textSize="10"
          h="50"
        />
      </vertical>

      <spinner
        id="chengyuan"
        textSize="10"
        layout_gravity="center"
        entries="选择|时9494|帆9031"
        textColor="#811c60"
        spinnerMode="dialog"
        h="*"
      />
      <vertical>
        <Switch
          id="changliang"
          text="屏幕长亮"
          layout_gravity="center"
          textColor="#4caf50"
          textSize="10"
          h="50"
        />
      </vertical>
    </linear>
    <card
      w="*"
      h="auto"
      margin="10 5"
      cardElevation="1dp"
      foreground="?selectableItemBackground"
    >
      <View bg="#d1d7ff" layout_gravity="left" h="*" w="1" />
      <View bg="#d1d7ff" layout_gravity="top" h="1" w="*" />
      <View bg="#d1d7ff" layout_gravity="right" h="*" w="1" />
      <text id="rizhi1" padding="5" textColor="#c9c9c9" textSize="17" />
      <ScrollView>
        <vertical>
          <text id="rizhi" padding="10" paddingTop="25" line="10" />
        </vertical>
      </ScrollView>
    </card>
  </vertical>
);

{
  /* <spinner
        id="bohaoka"
        textSize="10"
        entries="单卡  (小米)|卡1  (小米)|卡2  (小米)"
        textColor="#811c60"
        spinnerMode="dialog"
      /> */
}
/////////////////////////////////////////////////////////////
//界面读取
var storage = storages.create("存储界面");
var uichengyuan = storage.get("chengyuan");
var uiboda = storage.get("boda");
// var uibohaoka = storage.get("bohaoka");
if ((uichengyuan !== "") & (uichengyuan !== undefined) & (uichengyuan < ui.chengyuan.count)) {
  ui.chengyuan.setSelection(uichengyuan);
}
// if ((uibohaoka !== "") & (uibohaoka !== undefined)) {
//   ui.bohaoka.setSelection(uibohaoka);
// }
if ((uiboda !== "") & (uiboda !== undefined)) {
  // if (auto.service !== null) {
  ui.boda.checked = uiboda;
  // }
}
/////////////////////////////////////////////////////////////
//当应用后台时保存界面
ui.emitter.on("pause", () => {
  // storage.put("boda", ui.boda.checked);
  storage.put("chengyuan", ui.chengyuan.getSelectedItemPosition());
  // storage.put("bohaoka", ui.bohaoka.getSelectedItemPosition());
});
//退出程序结束动画
events.on("exit", function () {
  var len = animatorList.length;
  for (var i = 0; i < len; i++) {
    animatorList[i].cancel();
    //log("退出脚本, 中断动画: animator" + i);
  }
});
/////////////////////////////////////////////////////////////
//”日志输出“显示
ui.rizhi1.setText("日志输出：\n\n");
yemianrizhi = ui.rizhi.text();
ui.xinxi1.setText("小区(买卖) \n当前版本：" + version);
/////////////////////////////////////////////////////////////
//屏幕长亮
ui.changliang.on("check", (checked) => {
  if (checked == true) {
    device.keepScreenOn(360000000 * 1000);
  } else {
    device.cancelKeepingAwake();
  }
});
/////////////////////////////////////////////////////////////
//展示全部日志
ui.tolog.click(() => {
  app.startActivity("console");
});
/////////////////////////////////////////////////////////////
//删除日志
ui.clean.click(() => {
  ui.rizhi.setText("");
  yemianrizhi = "";
});
/////////////////////////////////////////////////////////////
//适配不同分辨率
if ((device.width == "1080") & (device.height == "2340")) {
} else if ((device.width == "1080") & (device.height == "1920")) {
  ui.zidong.textSize = 16;
  ui.shoudong.textSize = 16;
  ui.tolog.textSize = 10;
  ui.clean.textSize = 10;
  ui.tolog.attr("w", 45);
  ui.clean.attr("w", 45);
  // ui.bohaoka.attr("w", 95);
} else if ((device.width == "720") & (device.height == "1250")) {
} else if ((device.width == "1080") & (device.height == "2244")) {
} else {
  toastLog("界面可能存在错乱");
  threads.start(function () {
    qqSend(device.width + "*" + device.height);
  });
}
/////////////////////////////////////////////////////////////
//关闭音量+结束程序
$settings.setEnabled("stop_all_on_volume_up", false);
/////////////////////////////////////////////////////////////
//手动按钮点击执行
ui.shoudong.on("click", () => {
  //UI外多线程执行
  var thread1 = threads.start(function () {
    重新获取();
  });
});
/////////////////////////////////////////////////////////////
//自动判断执行
ui.zidong.on("check", (checked) => {
  if (checked == false) {
    ui.rizhi.setText("\n🤢🤢🤢已停止自动刷新🤢🤢🤢" + yemianrizhi);
    yemianrizhi = ui.rizhi.text();
    id2_on = false;
    ui.shoudong.clickable = true;
    setTimeout(() => {
      overColor(ui.zidong);
    }, 10);
  } else {
    setTimeout(() => {
      setTextPaintShaderGradientColor(ui.zidong);
    }, 10);
    ui.rizhi.setText("\n❤❤❤将开始自动刷新❤❤❤" + yemianrizhi);
    yemianrizhi = ui.rizhi.text();
    id2_on = true;
    threads.start(function () {
      ui.run(function () {
        ui.shoudong.clickable = false;
      });
      sleep(3000);
      ui.run(function () {
        ui.shoudong.clickable = true;
      });
    });
    var thread12 = threads.start(function () {
      while (id2_on == true) {
        try {
          重新获取();
          sleep(random(100, 500));
          if (id2_on == false) {
            break;
          }
        } catch (e) {}
      }
    });
  }
});
/////////////////////////////////////////////////////////////
//自动拨打按键触发
ui.boda.on("check", (checked) => {
  if (checked == false) {
    log("11110");
    setTimeout(() => {
      overColor(ui.boda);
    }, 10);
  } else {
    setTimeout(() => {
      setTextPaintShaderGradientColor(ui.boda);
    }, 10);
  }
});
/////////////////////////////////////////////////////////////
//点击拨打电话
ui.call.on("click", () => {
  if (id2_on) {
    ui.run(function () {
      ui.zidong.checked = false;
      ui.rizhi.setText("\n~~~~拨打电话，已关闭自动刷新" + yemianrizhi);
    });
    yemianrizhi = ui.rizhi.text();
  }
  if (num !== "") {
    let 选择 = ui.chengyuan.getSelectedItemPosition();
    if (选择 !== 0) {
      手机号 = chengyuan_phone[选择 - 1];
      token = chengyuan_token[选择 - 1];
      var thread12 = threads.start(function () {
        getphone();
        call();
      });
    } else {
      ui.rizhi.setText("\n😢😢😢还未选择自己的姓名😢😢" + yemianrizhi);
      yemianrizhi = ui.rizhi.text();
    }
  } else {
    ui.rizhi.setText("\n🙌🙌🙌还未获取到房源编号🙌🙌🙌" + yemianrizhi);
    yemianrizhi = ui.rizhi.text();
  }
});
/////////////////////////////////////////////////////////////
//获取房源信息
function 重新获取() {
  var 选择 = ui.chengyuan.getSelectedItemPosition();
  if (选择 == 0) {
    ui.run(function () {
      ui.rizhi.setText(
        "\n***请先选择姓名，否则无法app获取房源****" + yemianrizhi
      );
    });
    yemianrizhi = ui.rizhi.text();
  } else {
    手机号 = chengyuan_phone[选择 - 1];
    token = chengyuan_token[选择 - 1];
    //log(手机号);
    //log(token);
    if (apporweb == 0) {
      apporweb = 1;
      appGet();
    } else {
      apporweb = 0;
      webGet();
    }
  }
}
/////////////////////////////////////////////////////////////
//App获取
function appGet() {
  运行次数++;
  //获取的是房源列表
  for (let i = 0; i < 5; i++) {
    try {
      var temp = http
        .get(url1, {
          headers: {
            "X-House365-Client":
              "deviceid=E0%3ADC%3AFF%3AFB%3AA4%3AD3;phone=17512573036;uid=4126443;app_channel=%E5%8D%87%E7%BA%A7",
            "User-Agent":
              "365TaoFang/8.2.82 (Linux;U;Android11;Mi9Pro5GBuild/RKQ1.200826.002)",
            Host: "newrent.house365.com",
            Connection: "Keep-Alive",
            Cookie:
              "HouseKeywordSearchListsellnj=623b02348e90d1a5377c9ea85ff804f95f05b5e7a36ad23d7df8169c977c86e7a%3A2%3A%7Bi%3A0%3Bs%3A28%3A%22HouseKeywordSearchListsellnj%22%3Bi%3A1%3Bs%3A35%3A%22%5B%7B%22type%22%3A1%2C%22title%22%3A%22%E6%9D%A8%E6%9F%B3%E6%96%B0%E6%9D%91%22%7D%5D%22%3B%7D; website_jumpto_city=nj; _csrf-frontend=ef4359a8d8d018cbb8dc610d9b75068aa9431b11ad6d250257b69f392f1c86d9a%3A2%3A%7Bi%3A0%3Bs%3A14%3A%22_csrf-frontend%22%3Bi%3A1%3Bs%3A32%3A%222tOTb_jJnXlAQioLm5pkcUULvYgCbiAS%22%3B%7D",
          },
        })
        .body.json();
      break;
    } catch (error) {
      sleep(500);
      if (i > 3) {
        yemianrizhihanshu("\n" + 序号 + "\n网络存在问题");
        yemianrizhi = ui.rizhi.text();
      }
    }
  }
  xinxi = temp.data[0];
  //num=房源编号
  num = xinxi.id;
  log("房源编号：" + num);
  //信息=房源信息
  信息 = xinxi.title;
  yemian_信息 = "App " + 信息;

  if (信息.indexOf("室") > -1) {
    _信息 = 信息.split("室");
    if (_信息.length == 2) {
      _信息 = _信息[0];
      for (var i = 1; i < _信息.length; i++) {
        if (!_信息[_信息.length - i].match(/^[0-9]+.?[0-9]*$/)) {
          信息 =
            信息.substr(0, _信息.length - i + 1) +
            "\n" +
            信息.substr(_信息.length - i + 1, 信息.length);
          break;
        }
      }
    }
  }
  if (信息.indexOf("万元") > -1) {
    _信息 = 信息.split("万元");
    if (_信息.length == 2) {
      _信息 = _信息[0];
      for (var i = 1; i < _信息.length; i++) {
        if (!_信息[_信息.length - i].match(/^[0-9]+.?[0-9]*$/)) {
          if (_信息[_信息.length - i] != ".") {
            信息 =
              信息.substr(0, _信息.length - i + 1) +
              "  " +
              信息.substr(_信息.length - i + 1, 信息.length);
            break;
          }
        }
      }
    }
  }
  if (信息.indexOf("㎡") > -1) {
    _信息 = 信息.split("㎡");
    if (_信息.length == 2) {
      _信息 = _信息[0];
      for (var i = 1; i < _信息.length; i++) {
        if (!_信息[_信息.length - i].match(/^[0-9]+.?[0-9]*$/)) {
          if (_信息[_信息.length - i] != ".") {
            信息 =
              信息.substr(0, _信息.length - i + 1) +
              "  " +
              信息.substr(_信息.length - i + 1, 信息.length);
            break;
          }
        }
      }
    }
  }
  if (信息.indexOf("平") > -1) {
    _信息 = 信息.split("平");
    if (_信息.length == 2) {
      _信息 = _信息[0];
      for (var i = 1; i < _信息.length; i++) {
        if (!_信息[_信息.length - i].match(/^[0-9]+.?[0-9]*$/)) {
          if (_信息[_信息.length - i] != ".") {
            信息 =
              信息.substr(0, _信息.length - i + 1) +
              "  " +
              信息.substr(_信息.length - i + 1, 信息.length);
            break;
          }
        }
      }
    }
  }
  if (信息.indexOf("平方") > -1) {
    _信息 = 信息.split("平方");
    if (_信息.length == 2) {
      _信息 = _信息[0];
      for (var i = 1; i < _信息.length; i++) {
        if (!_信息[_信息.length - i].match(/^[0-9]+.?[0-9]*$/)) {
          if (_信息[_信息.length - i] != ".") {
            信息 =
              信息.substr(0, _信息.length - i + 1) +
              "  " +
              信息.substr(_信息.length - i + 1, 信息.length);
            break;
          }
        }
      }
    }
  }
  信息 = String(信息);
  姓名 = String(姓名);
  log("房源信息：" + yemian_信息);
  //姓名=业主姓名
  姓名 = xinxi.contactor;
  log("业主姓名：" + 姓名);

  //识别是否为首次刷新出
  识别重复();
}
/////////////////////////////////////////////////////////////
//WEB获取
function webGet() {
  运行次数++;
  for (let i = 0; i < 5; i++) {
    try {
      url = http.get(url2).body.string();
      break;
    } catch (e) {
      sleep(500);
      if (i > 3) {
        log("网络存在问题，未成功获取房源编号1");
        num = "";
      }
    }
  }
  if (url.indexOf('house-id="') > -1) {
    url = url.match(/house-id="(\S*)"/);
    num = url[1];
    url11 = "http://nj.sell.house365.com/s_" + num + ".html";
    log("房源编号：" + num);
    for (let i = 0; i < 5; i++) {
      try {
        url = http.get(url11).body.string();
        break;
      } catch (e) {
        sleep(500);
        if (i > 3) {
          log("网络存在问题，未成功获取房源编号1");
          num = "";
        }
      }
    }
    if (url.indexOf("<title>【") > -1) {
      姓名 = url.match(/<div class="adviserName fl">(\S*)</);
      if (url.indexOf("<title>【") > -1) {
        信息 = url.split("<title>【");
        信息 = 信息[1].split("_");
        信息 = 信息[0];
      }
      姓名 = 姓名[1];
      log("业主姓名：" + 姓名);
      yemian_信息 = "Web " + 信息;
      log("房源信息：" + yemian_信息);

      if (信息.indexOf("室") > -1) {
        _信息 = 信息.split("室");
        if (_信息.length == 2) {
          _信息 = _信息[0];
          for (var i = 1; i < _信息.length; i++) {
            if (!_信息[_信息.length - i].match(/^[0-9]+.?[0-9]*$/)) {
              信息 =
                信息.substr(0, _信息.length - i + 1) +
                "\n" +
                信息.substr(_信息.length - i + 1, 信息.length);
              break;
            }
          }
        }
      }
      if (信息.indexOf("万元") > -1) {
        _信息 = 信息.split("万元");
        if (_信息.length == 2) {
          _信息 = _信息[0];
          for (var i = 1; i < _信息.length; i++) {
            if (!_信息[_信息.length - i].match(/^[0-9]+.?[0-9]*$/)) {
              if (_信息[_信息.length - i] != ".") {
                信息 =
                  信息.substr(0, _信息.length - i + 1) +
                  "  " +
                  信息.substr(_信息.length - i + 1, 信息.length);
                break;
              }
            }
          }
        }
      }
      if (信息.indexOf("㎡") > -1) {
        _信息 = 信息.split("㎡");
        if (_信息.length == 2) {
          _信息 = _信息[0];
          for (var i = 1; i < _信息.length; i++) {
            if (!_信息[_信息.length - i].match(/^[0-9]+.?[0-9]*$/)) {
              if (_信息[_信息.length - i] != ".") {
                信息 =
                  信息.substr(0, _信息.length - i + 1) +
                  "  " +
                  信息.substr(_信息.length - i + 1, 信息.length);
                break;
              }
            }
          }
        }
      }
      if (信息.indexOf("平") > -1) {
        _信息 = 信息.split("平");
        if (_信息.length == 2) {
          _信息 = _信息[0];
          for (var i = 1; i < _信息.length; i++) {
            if (!_信息[_信息.length - i].match(/^[0-9]+.?[0-9]*$/)) {
              if (_信息[_信息.length - i] != ".") {
                信息 =
                  信息.substr(0, _信息.length - i + 1) +
                  "  " +
                  信息.substr(_信息.length - i + 1, 信息.length);
                break;
              }
            }
          }
        }
      }
      if (信息.indexOf("平方") > -1) {
        _信息 = 信息.split("平方");
        if (_信息.length == 2) {
          _信息 = _信息[0];
          for (var i = 1; i < _信息.length; i++) {
            if (!_信息[_信息.length - i].match(/^[0-9]+.?[0-9]*$/)) {
              if (_信息[_信息.length - i] != ".") {
                信息 =
                  信息.substr(0, _信息.length - i + 1) +
                  "  " +
                  信息.substr(_信息.length - i + 1, 信息.length);
                break;
              }
            }
          }
        }
      }
    } else {
      log("未找到编号1");
      num = "";
    }
  } else {
    log("未找到编号1");
    num = "";
  }
  //识别是否为首次刷新出
  识别重复();
}
/////////////////////////////////////////////////////////////
//识别房源是否重复
function 识别重复() {
  if (num == "") {
    //确认房源编号是否获取到
    log("未获取到房源编号" + 运行次数);
    yemianrizhihanshu("/n未获取到房源编号" + 运行次数);
  } else {
    if (运行次数 == 1) {
      //首次运行直接显示UI
      log("首次运行");
      显示UI();
      if (num_stroge.length < 1) {
        num_stroge[0] = num;
      }
      getphone();
      yemianrizhihanshu(
        "\n" +
          序号 +
          "\n房源编号:" +
          num +
          "\n小区信息:" +
          yemian_信息 +
          "\n业主姓名:" +
          姓名 +
          "\n虚拟号码:" +
          phone_num +
          yemianrizhi
      );
    } else {
      if (num_stroge.length < 1) {
        num_stroge[0] = num;
      } else {
        if (num_stroge.indexOf(num) > -1) {
          log("22");
          if (yemianrizhi.length > 2200) {
            yemianrizhi = "\n\n******已自动清理页面信息******";
          }
          yemianrizhihanshu(
            "\n" +
              序号 +
              "\n————重—复—房—源—————" +
              "\n房源编号:" +
              num +
              "\n小区信息:" +
              yemian_信息 +
              "\n业主姓名:" +
              姓名 +
              yemianrizhi
          );
        } else {
          log("获取到新房源");
          media.playMusic("./提醒.mp3");
          显示UI();
          getphone();
          num_stroge[num_stroge.length] = num;
          yemianrizhihanshu(
            "\n" +
              序号 +
              "\n房源编号:" +
              num +
              "\n小区信息:" +
              yemian_信息 +
              "\n业主姓名:" +
              姓名 +
              "\n虚拟号码:" +
              phone_num +
              yemianrizhi
          );
          if (ui.boda.checked) {
            call();
          }
        }
      }
    }
  }
}
/////////////////////////////////////////////////////////////
function 显示UI() {
  //如果都不为空则UI生成
  if ((num !== "") & (信息 !== "") & (姓名 !== "")) {
    ui.run(function () {
      ui.xinxi1.setText(信息);
      ui.xinxi2.setText(姓名);
    });
  }
  for (let i = 0; i < 5; i++) {
    try {
      url5 = http
        .get("http://nj.sell.house365.com/s_" + num + ".html")
        .body.string();
      //log(url5)
      break;
    } catch (e) {
      sleep(500);
      if (i > 3) {
        log("网络存在问题，未成功获取二维码");
      }
    }
  }
  if ((url5.indexOf("<img src='") > -1) & (url5.indexOf("'") > -1)) {
    image = url5.match(/<img src='(.+?)'/);
    image = image[1];
    if (image.indexOf("=") > -1) {
      image = image.split("=");
      image = image[0];
    }
    ui.run(function () {
      ui.tu.attr("src", image);
    });
    //log(image);
  } else {
    log("未找到imagebase");
  }
}
/////////////////////////////////////////////////////////////
//日志输出
function yemianrizhihanshu(X) {
  ui.run(function () {
    ui.rizhi.setText(X);
  });
  yemianrizhi = ui.rizhi.text();
  序号++;
}
/////////////////////////////////////////////////////////////
//UI显示虚拟手机号
function getphone() {
  phone_num = http
    .post(
      "http://newrent.house365.com/api/tf-app/get-secure-virtual-phone?city=nj&client=tf&deviceid=E0%3ADC%3AFF%3AFB%3AA4%3AD3&version=8.2.82&v=8.2.82&commitId=38c2795&api_key=android",
      {
        house_id: num,
        phone: 手机号,
        tbl: "sell",
      },
      {
        headers: {
          Respose_token_verify: "esf_token_sign",
          "X-House365-Client":
            "deviceid=E0%3ADC%3AFF%3AFB%3AA4%3AD3;phone=17512573036;uid=4126443;app_channel=%E5%8D%87%E7%BA%A7",
          "User-Agent":
            "365TaoFang/8.2.82 (Linux;U;Android11;Mi9Pro5GBuild/RKQ1.200826.002)",
          "esf-auth-token": token,
          "Content-Type": "application/x-www-form-urlencoded",
          "Content-Length": "45",
          Host: "newrent.house365.com",
          Connection: "Keep-Alive",
          Cookie:
            "website_jumpto_city=nj; _csrf-frontend=80e73b7d6cf6092fa02b5d1de34a5005d12fea4f4c0ddbe863e6aef80d1f6d2ca%3A2%3A%7Bi%3A0%3Bs%3A14%3A%22_csrf-frontend%22%3Bi%3A1%3Bs%3A32%3A%22TIhPnvTINWBp7CNx2ZLt4aVVOHspvC4l%22%3B%7D",
        },
      }
    )
    .body.json().data.virtual_phone;
  log(phone_num);
  //UI显示虚拟手机号
  if (phone_num.length == 11) {
    ui.run(function () {
      ui.phone_num.setText(phone_num);
    });
  } else {
    log("手机号获取错误：" + phone_num);
    ui.run(function () {
      ui.phone_num.setText("未获取到手机号");
    });
  }
}
/////////////////////////////////////////////////////////////
//call
function call() {
  if (phone_num.length == 11) {
    let call_intent = {
      action: "android.intent.action.CALL",
      data: "tel:" + phone_num,
    };
    try {
      app.startActivity(call_intent);
      // if (ui.boda.checked) {
      // tryyijianbohao();
      ui.run(function () {
        ui.zidong.checked = false;
        ui.rizhi.setText("\n~~~~拨打电话，已关闭自动刷新" + yemianrizhi);
      });
      yemianrizhi = ui.rizhi.text();
      // }
    } catch (e) {
      ui.run(function () {
        ui.rizhi.setText(
          "\n！！权限尚未开启！更新app或授予权限！！\n~~已复制App更新链接，请粘贴至浏览器下载~~\n" +
            yemianrizhi
        );
      });
      setClip(appUpdataUrl);
      yemianrizhi = ui.rizhi.text();
    }
  } else {
    toastLog("——还未获取手机号——");
  }
}

/////////////////////////////////////////////////////////////
//QQ推送消息
function qqSend(jihuo) {
  var dz = "https://qmsg.zendee.cn/send/d7e1af58bf94a6313992d2b97eeea497";
  for (let i = 0; i < 5; i++) {
    try {
      var 发送 = http.post(dz, {
        msg: jihuo,
        qq: "1101203976",
      });
      break;
    } catch (e) {
      sleep(500);
    }
  }
}
//文字流动效果
function setTextPaintShaderGradientColor(view) {
  var color1 = "#cc00ff";
  var color2 = "#ff9900";
  var color3 = "#4caf50";
  var color4 = "#cc00ff";
  let colorArr = util.java.array("int", 4);
  colorArr[0] = colors.parseColor(color1);
  colorArr[1] = colors.parseColor(color2);
  colorArr[2] = colors.parseColor(color3);
  colorArr[3] = colors.parseColor(color4);
  let wordWidth = new Packages.java.lang.Float(
    view.getPaint().getTextSize() * view.getText().length
  );
  //log("wordWidth = " + wordWidth);
  wordWidth = parseInt(Math.min(view.getWidth(), wordWidth));
  //log("wordWidth = " + wordWidth);
  let mLinearGradient = new LinearGradient(
    0,
    0,
    wordWidth,
    1,
    colorArr,
    [0, 0.3, 0.7, 1],
    Shader.TileMode.REPEAT
  );
  view.getPaint().setShader(mLinearGradient);
  view.invalidate();
  setTimeout(function () {
    createAnimator(view, mLinearGradient, wordWidth);
  }, 20);
}
//结束文字流动效果（待优化）
function overColor(view) {
  var color1 = "#4caf50";
  var color2 = "#4caf50";
  var color3 = "#4caf50";
  let colorArr = util.java.array("int", 3);
  colorArr[0] = colors.parseColor(color1);
  colorArr[1] = colors.parseColor(color2);
  colorArr[2] = colors.parseColor(color3);
  let wordWidth = new Packages.java.lang.Float(
    view.getPaint().getTextSize() * view.getText().length
  );
  //log("wordWidth = " + wordWidth);
  wordWidth = parseInt(Math.min(view.getWidth(), wordWidth));
  //log("wordWidth = " + wordWidth);
  let mLinearGradient = new LinearGradient(
    0,
    0,
    wordWidth,
    1,
    colorArr,
    [0, 0.5, 1],
    Shader.TileMode.REPEAT
  );
  view.getPaint().setShader(mLinearGradient);
  view.invalidate();
  setTimeout(function () {
    createAnimatorOver(view, mLinearGradient, wordWidth);
  }, 20);
}

//子函数不终结
function createAnimator(view, bgLinearGradient) {
  let viewWidth = view.getWidth();
  let wordWidth = new Packages.java.lang.Float(
    view.getPaint().getTextSize() * view.getText().length
  );
  let maxNum = Math.min(viewWidth, wordWidth);
  animator = ValueAnimator.ofInt(0, maxNum);
  let matrix = new Matrix();
  let mDx;
  animator.addUpdateListener(
    new ValueAnimator.AnimatorUpdateListener({
      onAnimationUpdate: function (animation) {
        mDx = animation.getAnimatedValue();
        matrix.setTranslate(mDx, 0);
        bgLinearGradient = view.getPaint().getShader();
        bgLinearGradient.setLocalMatrix(matrix);
        view.invalidate();
      },
    })
  );
  animator.setRepeatMode(ValueAnimator.RESTART);
  animator.setRepeatCount(ValueAnimator.INFINITE);
  animator.setStartDelay(500);
  animator.setDuration(3000);
  animator.start();
  //animatorList.push(animator);
}

//子函数1秒终结
function createAnimatorOver(view, bgLinearGradient) {
  let viewWidth = view.getWidth();
  let wordWidth = new Packages.java.lang.Float(
    view.getPaint().getTextSize() * view.getText().length
  );
  let maxNum = Math.min(viewWidth, wordWidth);
  animator = ValueAnimator.ofInt(0, maxNum);
  let matrix = new Matrix();
  let mDx;
  animator.addUpdateListener(
    new ValueAnimator.AnimatorUpdateListener({
      onAnimationUpdate: function (animation) {
        mDx = animation.getAnimatedValue();
        matrix.setTranslate(mDx, 0);
        bgLinearGradient = view.getPaint().getShader();
        bgLinearGradient.setLocalMatrix(matrix);
        view.invalidate();
      },
    })
  );
  animator.setRepeatMode(ValueAnimator.RESTART);
  animator.setRepeatCount(ValueAnimator.INFINITE);
  animator.setStartDelay(500);
  animator.setDuration(3000);
  animator.start();
  animatorList.push(animator);
  setTimeout(function () {
    animator.end();
  }, 1000);
}
